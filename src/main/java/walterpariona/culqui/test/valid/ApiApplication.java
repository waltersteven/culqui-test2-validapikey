package walterpariona.culqui.test.valid;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * Author: Walter Pariona 
 * */
// using SpringBoot
@SpringBootApplication
// Annotation to use Gson provided as a dependency
@EnableAutoConfiguration(exclude = {org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration.class})
public class ApiApplication {
	
	public static void main(String[] args) throws IOException {
		// Running Spring application
		SpringApplication.run(ApiApplication.class, args);		
		
		/* NOTE
		 * ENDPOINT: http://localhost:8081/apikey/validate 
		 * */

	}
}