package walterpariona.culqui.test.valid.model;

import javax.validation.constraints.NotBlank;

//Model which expose the response of the api
public class ResponseObject {
    @NotBlank
    private String valid;

    public ResponseObject(){}

    public ResponseObject(String valid) {
        this.valid = valid;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }
}