package walterpariona.culqui.test.valid.model;
import javax.validation.constraints.NotBlank;

//Model which maps the apikey value received
public class ApikeyObject {
    @NotBlank
    private String apikey;

    public ApikeyObject() {}
    
    public ApikeyObject(@NotBlank String apikey) {
        this.apikey = apikey;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }
}