package walterpariona.culqui.test.valid.controller;
import java.io.IOException;
import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import walterpariona.culqui.test.valid.model.ApikeyObject;
import walterpariona.culqui.test.valid.model.ResponseObject;
import javax.validation.Valid;

@RestController
@RequestMapping("/apikey")
public class ApiController {
	Boolean valid = false;
	ArrayList<ApikeyObject> arrayValues = new ArrayList<ApikeyObject>();

	public void createApiKeys() {
		arrayValues.add(new ApikeyObject("abc"));
		arrayValues.add(new ApikeyObject("cde"));
		arrayValues.add(new ApikeyObject("xyz"));
	}
	ResponseObject resobj;
	@PostMapping("/validate")
		public ResponseEntity<ResponseObject> postResponse(@Valid @RequestBody ApikeyObject apikey) throws IOException {
			createApiKeys();
			System.out.println("-------------> apikey value: " + apikey.getApikey());
			for (int i = 0; i < arrayValues.size(); i++) {
				if (arrayValues.get(i).getApikey().equalsIgnoreCase(apikey.getApikey())) {
					valid = true;
					break;
				}else {
					valid = false;
				}
			}

			resobj = new ResponseObject();
			
			//response of post request
			if (valid == false) {
				resobj.setValid("false");

				return ResponseEntity.ok().body(resobj);
			}else {
				resobj.setValid("true");
				// returns success response 
				return ResponseEntity.ok().body(resobj);

			}

			
		}
}
